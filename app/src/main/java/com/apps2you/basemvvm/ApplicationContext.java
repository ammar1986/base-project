package com.apps2you.basemvvm;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.apps2you.basemvvm.utils.AppLogger;
import com.facebook.drawee.backends.pipeline.Fresco;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;


public class ApplicationContext  extends Application {

    static ApplicationContext applicationContext;



    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        applicationContext = this;
        AppLogger.init();
        AndroidNetworking.initialize(this);
        if (BuildConfig.DEBUG) {
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY);
        }


        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/NotoSans-Regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
    }

    public static ApplicationContext getInstance(){
        return applicationContext;
    }
}
//Login page - line under register here should be thiner