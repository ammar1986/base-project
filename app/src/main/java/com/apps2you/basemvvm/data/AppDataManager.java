/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.apps2you.basemvvm.data;

import android.content.Context;


import com.apps2you.basemvvm.data.local.db.DbHelper;
import com.apps2you.basemvvm.data.local.prefs.PreferencesHelper;
import com.apps2you.basemvvm.data.remote.ApiHeader;
import com.apps2you.basemvvm.data.remote.ApiHelper;
import com.google.gson.Gson;


/**
 * Created by assaad on 07/07/17.
 */
public class AppDataManager implements DataManager {

    private final ApiHelper mApiHelper;

    private final DbHelper mDbHelper;

    private final PreferencesHelper mPreferencesHelper;

    private final Gson mGson;

    public AppDataManager(Context context, DbHelper dbHelper, PreferencesHelper preferencesHelper, ApiHelper apiHelper, Gson gson) {
        Context mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
        mGson = gson;
//        updateApiHeader(preferencesHelper.getCurrentUserId(), preferencesHelper.getAccessToken()); // TODO: 8/14/2019

    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }


//    public <T> Single<T> authenticatedSingleApi(Single<T> single) {
//        return single.onErrorResumeNext(refreshTokenAndRetryObser(single));
//    }
//
//
//    public <T> Function<Throwable, ? extends Single<? extends T>> refreshTokenAndRetryObser(final Single<T> toBeResumed) {
//        return (Function<Throwable, Single<? extends T>>) throwable -> {
//            if (ExceptionStatusCode.isHttp401Error(throwable)) {
//                mPreferencesHelper.setRefreshingToken(true);
//                return refreshToken().flatMap(refreshTokenResponse -> {
//                    mPreferencesHelper.setRefreshingToken(false);
//                    mPreferencesHelper.setAccessToken(refreshTokenResponse.getToken());
//                    return toBeResumed;
//                });
//            }
//            return Single.error(throwable);
//        };


    }







