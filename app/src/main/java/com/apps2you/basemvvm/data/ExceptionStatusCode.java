package com.apps2you.basemvvm.data;

import com.androidnetworking.error.ANError;

public  class ExceptionStatusCode{
         public static Boolean isHttp401Error(Throwable throwable){
             return ((ANError)throwable).getErrorCode() == 401;
         }
    }