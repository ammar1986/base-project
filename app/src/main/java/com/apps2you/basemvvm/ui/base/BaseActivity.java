/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.apps2you.basemvvm.ui.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;


import com.apps2you.basemvvm.BR;
import com.apps2you.basemvvm.ViewModelProviderFactory;
import com.apps2you.basemvvm.data.AppDataManager;
import com.apps2you.basemvvm.data.local.db.AppDatabase;
import com.apps2you.basemvvm.data.local.db.AppDbHelper;
import com.apps2you.basemvvm.data.local.prefs.AppPreferencesHelper;
import com.apps2you.basemvvm.data.remote.ApiHeader;
import com.apps2you.basemvvm.data.remote.AppApiHelper;
import com.apps2you.basemvvm.ui.base.core.LocalizationActivity;
import com.apps2you.basemvvm.ui.dialogs.ErrorDialog;
import com.apps2you.basemvvm.ui.dialogs.InformDialogInterface;
import com.apps2you.basemvvm.utils.AppConstants;
import com.apps2you.basemvvm.utils.CommonUtils;
import com.apps2you.basemvvm.utils.NetworkUtils;
import com.apps2you.basemvvm.utils.rx.AppSchedulerProvider;
import com.google.gson.Gson;


/**
 * Created by assaad on 07/07/17.
 */

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends LocalizationActivity
        implements BaseFragment.Callback, InformDialogInterface {




    @Override
    protected void onDestroy() {
        super.onDestroy();
    }





    // TODO
    // this can probably depend on isLoading variable of BaseViewModel,
    // since its going to be common for all the activities
    private ProgressDialog mProgressDialog;
    private T mViewDataBinding;
    private V mViewModel;

    public ViewModelProviderFactory factory;
    /**
     * Override for set binding variable
     *
     * @return variable id
     */


    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }


    public String getStringResource(int id) {
        return getString(id);
    }

    @Override
    protected void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        factory = new ViewModelProviderFactory(new AppDataManager(this,
                new AppDbHelper(AppDatabase.getDatabase(this)),
                new AppPreferencesHelper(this, AppConstants.PREF_NAME),
                new AppApiHelper(new ApiHeader(new ApiHeader.PublicApiHeader(""), new ApiHeader.ProtectedApiHeader("", 0L, ""))),
                new Gson()), new AppSchedulerProvider());
        performDataBinding();



    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        return view!=null;
    }

    public boolean hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
        return view!=null;
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    public void openActivityOnTokenExpire() {
//        startActivity(RawaLoginActivity.newIntent(this));
        finish();
    }


    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(BR.viewModel, mViewModel);
        mViewDataBinding.executePendingBindings();
        mViewDataBinding.setLifecycleOwner(this);
    }

    public void informUser(String message, int redirection) {
        ErrorDialog.newInstance(message, redirection).show(getSupportFragmentManager(), "");
    }

    public void informUser(int message, int redirection) {
        ErrorDialog.newInstance(getString(message), redirection).show(getSupportFragmentManager(), "");
    }

    public void informUser(int message) {
        ErrorDialog.newInstance(getString(message), ErrorDialog.NO_REDIRECTION).show(getSupportFragmentManager(), "");
    }

    public void informUser(String message) {
        ErrorDialog.newInstance(message, ErrorDialog.NO_REDIRECTION).show(getSupportFragmentManager(), "");
    }


    @Override
    public void onInfoDialogOkClicked(String s, int redirection) {


    }

    public boolean handleError(Throwable throwable) {
        if (NetworkUtils.isNetworkConnected(this)) {
            return true;
        } else
        return false;
    }

    public boolean informNoConnection() {
        if (NetworkUtils.isNetworkConnected(this)) {
            return true;
        } else return false;
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}

