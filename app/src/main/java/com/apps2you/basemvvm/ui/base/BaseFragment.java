/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.apps2you.basemvvm.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.apps2you.basemvvm.R;
import com.apps2you.basemvvm.ViewModelProviderFactory;
import com.apps2you.basemvvm.data.AppDataManager;
import com.apps2you.basemvvm.data.local.db.AppDatabase;
import com.apps2you.basemvvm.data.local.db.AppDbHelper;
import com.apps2you.basemvvm.data.local.prefs.AppPreferencesHelper;
import com.apps2you.basemvvm.data.remote.ApiHeader;
import com.apps2you.basemvvm.data.remote.AppApiHelper;
import com.apps2you.basemvvm.ui.dialogs.ErrorDialog;
import com.apps2you.basemvvm.ui.dialogs.InformDialogInterface;
import com.apps2you.basemvvm.utils.AppConstants;
import com.apps2you.basemvvm.utils.NetworkUtils;
import com.apps2you.basemvvm.utils.rx.AppSchedulerProvider;
import com.google.gson.Gson;

/**
 * Created by assaad on 09/07/17.
 */

public abstract class BaseFragment<T extends ViewDataBinding, V extends BaseViewModel> extends Fragment implements InformDialogInterface {

    public ViewModelProviderFactory factory;
    private BaseActivity mActivity;
    private View mRootView;
    private T mViewDataBinding;
    private V mViewModel;

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }
    }

    @Override
    public void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        factory = new ViewModelProviderFactory(new AppDataManager(getActivity(),
                new AppDbHelper(AppDatabase.getDatabase(getActivity())),
                new AppPreferencesHelper(getActivity(), AppConstants.PREF_NAME),
                new AppApiHelper(new ApiHeader(new ApiHeader.PublicApiHeader(""), new ApiHeader.ProtectedApiHeader("", 0L, ""))),
                new Gson()), new AppSchedulerProvider());
        mViewModel = getViewModel();
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(@androidx.annotation.NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        mRootView = mViewDataBinding.getRoot();
        return mRootView;
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(@androidx.annotation.NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.setLifecycleOwner(this);
        mViewDataBinding.executePendingBindings();
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public void hideKeyboard() {
        if (mActivity != null) {
            mActivity.hideKeyboard();
        }
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }

    public void openActivityOnTokenExpire() {
        if (mActivity != null) {
            mActivity.openActivityOnTokenExpire();
        }
    }


    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }


    public void informUser(String message, int redirection) {
        ErrorDialog.newInstance(message, redirection).show(getActivity().getSupportFragmentManager(), "");
    }

    public void informUser(int message, int redirection) {
        ErrorDialog.newInstance(getString(message), redirection).show(getActivity().getSupportFragmentManager(), "");
    }

    public void informUser(int message) {
        ErrorDialog.newInstance(getString(message), ErrorDialog.NO_REDIRECTION).show(getActivity().getSupportFragmentManager(), "");
    }

    public void informUser(String message) {
        ErrorDialog.newInstance(message, ErrorDialog.NO_REDIRECTION).show(getActivity().getSupportFragmentManager(), "");
    }

    public String getStringResource(int id) {
        return getString(id);
    }

    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(int message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public boolean handleError(Throwable throwable) {
        if (NetworkUtils.isNetworkConnected(getActivity())) {
            return true;
        } else {
            informUser(R.string.no_internet_connection, NetworkUtils.REDIRECTION_NO_INTERNET_CONNECTION);
            return false;
        }
    }

    public boolean informNoConnection() {
        if (NetworkUtils.isNetworkConnected(getActivity())) {
            return true;
        } else {
            informUser(R.string.no_internet_connection);
            return false;
        }
    }
    @Override
    public void onInfoDialogOkClicked(String s, int redirection) {
        {
            if (redirection == NetworkUtils.REDIRECTION_NO_INTERNET_CONNECTION)
                getActivity().finish();
        }
    }

}
