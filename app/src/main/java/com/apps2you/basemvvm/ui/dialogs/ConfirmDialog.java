package com.apps2you.basemvvm.ui.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.apps2you.basemvvm.R;
import com.apps2you.basemvvm.databinding.ConfirmDialogBinding;


public class ConfirmDialog extends DialogFragment {




    private final String ARG_MESSAGE = "ARG_MESSAGE";
    private final String ARG_REDIRECTION = "ARG_REDIRECTION";

    private ConfirmDialogBinding mDialogConfirmBinding;


    public static ConfirmDialog newInstance(String msg, int redirectionType) {
        ConfirmDialog fragment = new ConfirmDialog();
        Bundle args = new Bundle();
        args.putString(fragment.ARG_MESSAGE, msg);
        args.putInt(fragment.ARG_REDIRECTION, redirectionType);
        fragment.setArguments(args);
        return fragment;

    }

    public static ConfirmDialog newInstance(String msg, String okText, String cancelText) {
        ConfirmDialog fragment = new ConfirmDialog();
        Bundle args = new Bundle();
        args.putString(fragment.ARG_MESSAGE, msg);
        args.putString(fragment.ARG_OK, okText);
        args.putString(fragment.ARG_CANCEL, cancelText);
        fragment.setArguments(args);
        return fragment;

    }

    public static ConfirmDialog newInstance(String msg) {
        ConfirmDialog fragment = new ConfirmDialog();
        Bundle args = new Bundle();
        args.putString(fragment.ARG_MESSAGE, msg);
        fragment.setArguments(args);
        return fragment;

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    int getLayoutId() {
        return R.layout.confirm_dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        mDialogConfirmBinding = DataBindingUtil.inflate(inflater, getLayoutId(), null, false);
        return mDialogConfirmBinding.getRoot();

    }


    private static final String ARG_OK = "ARG_OK";
    private static final String ARG_CANCEL = "ARG_CANCEL";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mDialogConfirmBinding.tvDescription.setText(getArguments().getString(ARG_MESSAGE));
        }

        mDialogConfirmBinding.tvContinue.setOnClickListener(view1 -> {
            if(welcomeDialogInterface!=null)
                welcomeDialogInterface.onConfirmDialogOkClicked();

            dismiss();
        });

        mDialogConfirmBinding.tvContinue.setText(getArguments().getString(ARG_OK,getString(R.string.ok)));
        mDialogConfirmBinding.tvCancel.setText(getArguments().getString(ARG_CANCEL,getString(R.string.cancel)));

        mDialogConfirmBinding.tvCancel.setOnClickListener(view1 -> {
            if(welcomeDialogInterface!=null)
                welcomeDialogInterface.onConfirmDialogCancelClicked();

            dismiss();
        });

    }


    public void setListener(ConfirmDialogInterface welcomeDialogInterface){
        this.welcomeDialogInterface = welcomeDialogInterface;
    }
    ConfirmDialogInterface welcomeDialogInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        welcomeDialogInterface = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
