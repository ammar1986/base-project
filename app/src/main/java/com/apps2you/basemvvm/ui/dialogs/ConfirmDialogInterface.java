package com.apps2you.basemvvm.ui.dialogs;

public interface ConfirmDialogInterface {
    void onConfirmDialogOkClicked();
    void onConfirmDialogCancelClicked();
}
