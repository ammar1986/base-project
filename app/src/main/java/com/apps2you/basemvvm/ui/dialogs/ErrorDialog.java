package com.apps2you.basemvvm.ui.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.apps2you.basemvvm.R;
import com.apps2you.basemvvm.databinding.ErrorDialogBinding;


public class ErrorDialog extends DialogFragment {

    public static final int NO_REDIRECTION = -1;



    private final String ARG_MESSAGE = "ARG_MESSAGE";
    private final String ARG_REDIRECTION = "ARG_REDIRECTION";

    private ErrorDialogBinding mDialogErrorBinding;


    public static ErrorDialog newInstance(String msg, int redirectionType) {
        ErrorDialog fragment = new ErrorDialog();
        Bundle args = new Bundle();
        args.putString(fragment.ARG_MESSAGE, msg);
        args.putInt(fragment.ARG_REDIRECTION, redirectionType);
        fragment.setArguments(args);
        return fragment;

    }

    public static ErrorDialog newInstance(String msg) {
        ErrorDialog fragment = new ErrorDialog();
        Bundle args = new Bundle();
        args.putString(fragment.ARG_MESSAGE, msg);
        args.putInt(fragment.ARG_REDIRECTION, NO_REDIRECTION);
        fragment.setArguments(args);
        return fragment;

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    int getLayoutId() {
        return R.layout.error_dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        mDialogErrorBinding = DataBindingUtil.inflate(inflater, getLayoutId(), null, false);
        return mDialogErrorBinding.getRoot();

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mDialogErrorBinding.tvDescription.setText(getArguments().getString(ARG_MESSAGE));
        }

        mDialogErrorBinding.tvContinue.setOnClickListener(view1 -> {
            if(welcomeDialogInterface!=null)
                welcomeDialogInterface.onInfoDialogOkClicked(getArguments().getString(ARG_MESSAGE), getArguments().getInt(ARG_REDIRECTION,ErrorDialog.NO_REDIRECTION));

            dismiss();
        });

    }

    InformDialogInterface welcomeDialogInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InformDialogInterface) {
            welcomeDialogInterface = (InformDialogInterface) context;
        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        welcomeDialogInterface = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
