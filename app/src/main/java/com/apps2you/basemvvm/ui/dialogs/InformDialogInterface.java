package com.apps2you.basemvvm.ui.dialogs;

public interface InformDialogInterface {
    void onInfoDialogOkClicked(String s, int redirection);
}
