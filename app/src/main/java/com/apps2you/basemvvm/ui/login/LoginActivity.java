package com.apps2you.basemvvm.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.lifecycle.ViewModelProviders;

import com.apps2you.basemvvm.BR;
import com.apps2you.basemvvm.R;
import com.apps2you.basemvvm.databinding.ActivityRawaLoginBinding;
import com.apps2you.basemvvm.ui.base.BaseActivity;
import com.apps2you.basemvvm.ui.dialogs.ErrorDialog;

public class LoginActivity extends BaseActivity<ActivityRawaLoginBinding, LoginViewModel> implements LoginNavigator {

    private LoginViewModel mRawaLoginViewModel;
    private ActivityRawaLoginBinding mActivityRawaLoginBinding;

    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_rawa_login;
    }

    @Override
    public LoginViewModel getViewModel() {
        mRawaLoginViewModel = ViewModelProviders.of(this, factory).get(LoginViewModel.class);
        return mRawaLoginViewModel;
    }

    @Override
    public boolean handleError(Throwable throwable) {
        return super.informNoConnection();
    }

    @Override
    public void goToRegistration() {
    }

    @Override
    public void goToMainPage() {
    }

    @Override
    public void goToForgotPassword() {
    }

    @Override
    public void informUser(String msg) {
        ErrorDialog.newInstance(msg, ErrorDialog.NO_REDIRECTION).show(getSupportFragmentManager(), "");
    }

    @Override
    public void onNickNameError() {
        mActivityRawaLoginBinding.etEmail.setError("field_is_required");
    }

    @Override
    public void onPasswordError() {
        mActivityRawaLoginBinding.etPassword.setError("field_is_required");
    }

    @Override
    public void goToSupport() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityRawaLoginBinding = getViewDataBinding();
        mActivityRawaLoginBinding.setVariable(BR.navigator, this);
        mRawaLoginViewModel.setNavigator(this);

        mRawaLoginViewModel.setObserbables(this);


    }


    @Override
    public void onInfoDialogOkClicked(String s, int redirection) {
        switch (redirection) {

        }

    }

    @Override
    public void goToChangePassword(String email) {
        informUser("message_has_been_send__to_ur_email" + "\n" + mRawaLoginViewModel.userEmail.getValue(), 398467);
    }

}




