/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.apps2you.basemvvm.ui.login;

import android.text.TextUtils;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.androidnetworking.error.ANError;
import com.apps2you.basemvvm.data.DataManager;
import com.apps2you.basemvvm.ui.base.BaseViewModel;
import com.apps2you.basemvvm.utils.rx.SchedulerProvider;

import org.json.JSONObject;

/**
 * Created by assaad ammar on 03/07/19.
 */

public class LoginViewModel extends BaseViewModel<LoginNavigator> {

    public MutableLiveData<String> userEmail;
    public MutableLiveData<String> userPassword;
    private MutableLiveData<Boolean> mEmailError;
    private final ObservableBoolean mIsValidEmail = new ObservableBoolean();
    private final ObservableBoolean mIsAllValid = new ObservableBoolean();

    private final MutableLiveData informUserEvent = new MutableLiveData();

    public final ObservableBoolean isShowError;
    public MutableLiveData<String> errorMessage;

    public ObservableBoolean getIsAllValid() {
        return mIsAllValid;
    }

    public LoginViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        mEmailError = new MutableLiveData<>();
        userEmail = new MutableLiveData<>();
        userPassword = new MutableLiveData<>();
        isShowError = new ObservableBoolean();
        errorMessage = new MutableLiveData<>();


        userEmail.setValue("");
        userPassword.setValue("");
        setIsValidEmail(true);
        isShowError.set(false);

    }

    public void setObserbables(LifecycleOwner owner) {
        userEmail.observe(owner, s -> {
            checkIfAllIsValid();
        });
        userPassword.observe(owner, s -> {
            checkIfAllIsValid();
        });
    }




    public void checkIfAllIsValid(){
        mIsAllValid.set(!TextUtils.isEmpty(userEmail.getValue()) && userEmail.getValue().length() > 3 && userPassword.getValue().length() > 7);
    }


    public ObservableBoolean getIsValidEmail() {
        return mIsValidEmail;
    }

    public void setIsValidEmail(boolean isValidEmail) {
        mIsValidEmail.set(isValidEmail);
    }


    private boolean isLoginValid() {
        boolean isValid = true;
        if (TextUtils.isEmpty(userEmail.getValue())) {
            getNavigator().onNickNameError();
            isValid = false;
        }
        if (TextUtils.isEmpty(userPassword.getValue())) {
            getNavigator().onPasswordError();
            isValid = false;
        }

        return isValid;
    }

    public void onLoginClicked() {

        if (!isLoginValid())
            return;
        setIsLoading(true);
         getCompositeDisposable().add(getDataManager().doSignInApiCall()
                 .subscribeOn(getSchedulerProvider().io())
                 .observeOn(getSchedulerProvider().ui())
                 .doOnSuccess(response -> {
//                     getDataManager().updateUserInfo(response, DataManager.LoggedInMode.LOGGED_IN_MODE_SERVER);
                 })
                 .subscribe(response -> {
                     setIsLoading(false);
                     getNavigator().goToMainPage();
                 }, throwable -> {
                     setIsLoading(false);
                     switch (((ANError) throwable).getErrorCode()) {
                         case 400:
                             String response = ((ANError) throwable).getErrorBody();
                             JSONObject jsonObject = new JSONObject(response);
                             String message = getErrorMessage(jsonObject.optString("data"));
                             if (!TextUtils.isEmpty(message))
                                 getNavigator().informUser(message);
                             getInformUserEvent().setValue(message);
                             break;
                         default:
                             getNavigator().handleError(throwable);

                     }

                 }));

    }

    public void onRegisterClicked() {
        getNavigator().goToRegistration();




    }


    public void onForgotPassowrdClicked() {
        getNavigator().goToForgotPassword();
    }


    private String getErrorMessage(String message) {
        return "";
    }


    public void forgotPassword() {

    }


    public MutableLiveData getInformUserEvent() {
        return informUserEvent;
    }
}
