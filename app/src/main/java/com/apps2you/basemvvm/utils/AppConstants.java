/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.apps2you.basemvvm.utils;


import com.apps2you.basemvvm.ApplicationContext;
import com.apps2you.basemvvm.R;

/**
 * Created by assaad on 07/07/17.
 */

public final class AppConstants {

    public static final String LINK_DOCUMENTS_GOOGLE_WEB = "https://docs.google.com/gview?embedded=true&url=";

    public static final String BASE_LINK_BROWSER = "https://staging.rawa.tv/";

    public static final String TEMP_MEDIA_FILE_PATH = ApplicationContext.getInstance().getString(R.string.app_name) + "/";
    public static final String folderPath = TEMP_MEDIA_FILE_PATH + "files/pictures/";

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String DB_NAME = "rawa.db";

    public static final long NULL_INDEX = -1L;

    public static final String PREF_NAME = "rawa_pref";

    public static final String STATUS_CODE_FAILED = "failed";

    public static final String STATUS_CODE_SUCCESS = "success";

    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";

    public static final int LANGUAGE_AR = 1;

    public static final int LANGUAGE_EN = 2;

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String DATE_FORMAT_EVENTS = "yyyy-MM-dd hh:mm:ss";

    public static final String PROVIDER_FACEBOOK = "facebook";

    public static final String PROVIDER_GOOGLE = "google";


    public static final int REQ_CODE_CROP_IMAGE = 2;

    public static final int REQ_CODE_GET_IMAGE = 3;

    public static final int REQ_CODE_GET_VIDEO = 4;

    public static final int REQ_CODE_GET_IMAGE_PROFILE = 5;

    public static final int REQ_CODE_GET_IMAGE_BANNER = 6;

    public static final int REQ_CODE_CROP_IMAGE_PROFILE = 7;

    public static final int REQ_CODE_CROP_IMAGE_BANNER = 8;


    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
