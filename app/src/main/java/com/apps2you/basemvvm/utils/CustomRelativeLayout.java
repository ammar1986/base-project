package com.apps2you.basemvvm.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.apps2you.basemvvm.ui.base.BaseActivity;


public class CustomRelativeLayout extends RelativeLayout {
    public CustomRelativeLayout(Context context) {
        super(context);
    }

    public CustomRelativeLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomRelativeLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        setLayoutParams(new RelativeLayout.LayoutParams(w, h));
    }

    public void setHeight(int w, int h, int oldw, int oldh){
        onSizeChanged(w, h, oldw, oldh);
    }

    public void setRatioBasedOnHeight(BaseActivity context, float ratio){
        float newWidth, originalWidth, originalHeight;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        originalWidth = getWidth();
        originalHeight = displayMetrics.heightPixels;
        newWidth =  originalHeight / ratio;
        setHeight((int)newWidth, (int)originalHeight,(int) originalWidth, (int)originalHeight);
    }

    public void setRatioBasedOnHeight(FragmentActivity context, float ratio){
        float newWidth, originalWidth, originalHeight;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        originalWidth = getWidth();
        originalHeight = displayMetrics.heightPixels;
        newWidth =  originalHeight / ratio;
        setHeight((int)newWidth, (int)originalHeight,(int) originalWidth, (int)originalHeight);
    }


    public void setRatioBasedOnWidth(BaseActivity context, float ratio){
        float newHeight, originalWidth, originalHeight;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        originalWidth = displayMetrics.widthPixels;
        originalHeight = getHeight();
        newHeight =  originalWidth *ratio;
        setHeight((int)originalWidth, (int)newHeight,(int) originalWidth, (int)originalHeight);
    }
    public void setRatioBasedOnWidth(FragmentActivity context, float ratio){
        float newHeight, originalWidth, originalHeight;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        originalWidth = displayMetrics.widthPixels;
        originalHeight = getHeight();
        newHeight =  originalWidth *ratio;
        setHeight((int)originalWidth, (int)newHeight,(int) originalWidth, (int)originalHeight);
    }
    public void setHeightFullWidth(BaseActivity context,int height){
        int newHeight, originalWidth, originalHeight;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        originalWidth = displayMetrics.widthPixels;
        setHeight(originalWidth, height, originalWidth, getHeight());
    }

    public void setHeightFullWidth(FragmentActivity context,int height){
        int newHeight, originalWidth, originalHeight;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        originalWidth = displayMetrics.widthPixels;
        setHeight(originalWidth, height, originalWidth, getHeight());
    }
}
