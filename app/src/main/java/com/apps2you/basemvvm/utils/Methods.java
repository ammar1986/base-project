package com.apps2you.basemvvm.utils;

import android.os.Environment;
import android.util.Log;

import com.apps2u.HappyDB.encryption.b64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Methods {

    public static String getFormattedDateFromTimestamp(long timestampInMilliSeconds) {


        SimpleDateFormat formattedDate = new SimpleDateFormat(AppConstants.DATE_FORMAT, Locale.ENGLISH);
        formattedDate.setTimeZone(TimeZone.getDefault());
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String dateResult = formattedDate.format(date);

        return dateResult;

    }
    public static String getFormattedDateFromTimestamp(long timestampInMilliSeconds, String format) {

        SimpleDateFormat formattedDate = new SimpleDateFormat(format, Locale.ENGLISH);
        formattedDate.setTimeZone(TimeZone.getDefault());
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String dateResult = formattedDate.format(date);
        return dateResult;

    }

    public static String getFormattedDateFromTimestampLocale(long timestampInMilliSeconds, String format) {

        SimpleDateFormat formattedDate = new SimpleDateFormat(format);
        formattedDate.setTimeZone(TimeZone.getDefault());
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String dateResult = formattedDate.format(date);
        return dateResult;

    }

    public static Long convertDateToMilliseconds(String date, String format) {

        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date mDate = sdf.parse(date);

            long timeInMilliseconds = mDate.getTime();
            return timeInMilliseconds;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static int getLanguageOfApp() {
        return Locale.getDefault().getLanguage().equals("ar") ? AppConstants.LANGUAGE_AR : AppConstants.LANGUAGE_EN;
    }


    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }



    private static String generateHashWithHmac256(String message, String key) {
        String messageDigest = "";
        try {
            final String hashingAlgorithm = "HmacSHA1"; //or "HmacSHA256", "HmacSHA1", "HmacSHA512"

            byte[] bytes = hmac(hashingAlgorithm, key.getBytes(), message.getBytes());

            messageDigest = b64.encode1(bytes);

            Log.i("generateHashWithHmac256", "message digest: " + messageDigest);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageDigest.replaceAll("\\/", "_").replaceAll("\\+", "-");
    }

    public static byte[] hmac(String algorithm, byte[] key, byte[] message) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(message);
    }

    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0, v; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }



    public static float getAspectRatioForGames() {
        return 0.85f;
    }
    public static float getAspectRatioForChannels() {
        return 1f;
    }

    public static File createMediaFile(String filePath, String ext) throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp;

        File storageDir = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
            storageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), filePath);
        }

        if (!storageDir.exists())
            storageDir.mkdirs();

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ext,         /* suffix */
                storageDir      /* directory */
        );

        if (!image.exists())
            image.mkdirs();


        return image;
    }

    public static String createMediaPath(String filePath, String ext) throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp;

        File storageDir = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
            storageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), filePath);
        }

        if (!storageDir.exists())
            storageDir.mkdirs();

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ext,         /* suffix */
                storageDir      /* directory */
        );

        return image.getPath();
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

}
