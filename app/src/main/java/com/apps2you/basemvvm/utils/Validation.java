package com.apps2you.basemvvm.utils;

import com.androidnetworking.error.ANError;
import com.google.gson.Gson;

import java.util.regex.Pattern;

public class Validation {

    public static boolean isValidUserName(String userName){
        return Pattern.compile("^[a-zA-Z0-9_]{4,15}$").matcher(userName).matches();
    }



}
