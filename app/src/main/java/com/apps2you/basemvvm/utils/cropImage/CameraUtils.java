package com.apps2you.basemvvm.utils.cropImage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.Surface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

/**
 * Created by Doumith Awkar on 8/16/2016.
 */
public class CameraUtils
{


    private final static double epsilon = 0.5;


    public static List<Size> getAvailableCameraSizes(Camera camera)
    {
        List<Size> list;
        Size selectedSize = null;
        if (camera.getParameters().getSupportedVideoSizes() != null) {
            list = camera.getParameters().getSupportedVideoSizes();
        } else {
            // Video sizes may be null, which indicates that all the supported
            // preview sizes are supported for video recording.
            list = camera.getParameters().getSupportedPreviewSizes();
        }

        return list;
    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera)
    {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);

        Camera.Parameters params = camera.getParameters();
        params.setRotation(result);
        camera.setParameters(params);
    }


    public static int getDisplayOrientation(Activity activity, int cameraId, Camera camera)
    {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    public static Size getOptimalPreviewSize(Camera camera, int w, int h)
    {
        List<Size> sizes = getAvailableCameraSizes(camera);
        final double ASPECT_TOLERANCE = 0.4;
        double targetRatio = (double) h / w;

        if (sizes == null) return null;

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public static Size getCameraSize(Camera camera)
    {
        int m_height = 1080;
        int m_width = 640;
//        int m_height = 540;
//        int m_width = 320;
        List<Size> list;
        Size selectedSize = null;
        if (camera.getParameters().getSupportedVideoSizes() != null) {
            list = camera.getParameters().getSupportedVideoSizes();
        } else {
            list = camera.getParameters().getSupportedPreviewSizes();
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).height == m_height && list.get(i).width == m_width) {
                selectedSize = list.get(i);
                break;
            } else {
                if (list.get(i).height <= m_height && list.get(i).width <= m_width) {
                    selectedSize = list.get(i);
                    break;
                }
            }
        }
        return selectedSize;
    }

    public static Bitmap correctRotation(String path, Bitmap srcBitmap)
        {
            Bitmap rotatedImage = null;
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(path);
            } catch (Exception e) {
                e.printStackTrace();
                return srcBitmap;
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedImage = rotateImage(srcBitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedImage = rotateImage(srcBitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedImage = rotateImage(srcBitmap, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    rotatedImage = srcBitmap;
                    break;

                default:
                    rotatedImage = srcBitmap;
                break;
        }
        return rotatedImage;
    }

    public static Bitmap rotateImage(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public static Bitmap getBitmapFromPath(String filePath)
    {
        File imgFile = new File(filePath);
        if (imgFile.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            return bitmap;
        }
        return null;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
