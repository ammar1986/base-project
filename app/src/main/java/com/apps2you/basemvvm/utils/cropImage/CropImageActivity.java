package com.apps2you.basemvvm.utils.cropImage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.apps2you.basemvvm.R;
import com.apps2you.basemvvm.utils.AppConstants;
import com.apps2you.basemvvm.utils.Methods;
import com.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by AppsBassel on 10/17/2017.
 */

public class CropImageActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = CropImageActivity.class.getSimpleName();

    private static final int REQ_CODE_SEND_IMAGE = 1001;

    public static final String ARG_RATIO_X = "REQ_CODE_RATIO_X";
    public static final String ARG_RATIO_Y = "REQ_CODE_RATIO_Y";

    public static final String BUNDLE_IMAGE_FILE_PATH = "image_file_path";
    public static final String ARG_IS_WITHCAPTION = "ARG_IS_WITHCAPTION";
    private CropImageView ivCrop;
    private String filePath;

    private AppCompatTextView tvCancel, tvDone;

    private RelativeLayout rlProgressBar;

    private RectF cropRect;

    private boolean isWithCaption = true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);

        tvCancel =  findViewById(R.id.tv_cancel);
        tvDone =  findViewById(R.id.tv_done);

        ivCrop =  findViewById(R.id.crop_image_view);
        rlProgressBar =  findViewById(R.id.rl_progress);
        setupCropImageView();


        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                filePath = bundle.getString(BUNDLE_IMAGE_FILE_PATH);
                isWithCaption = bundle.getBoolean(ARG_IS_WITHCAPTION, true);
                initCrop(filePath);
            }
        }
        tvCancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancel:
                finish();
                break;
            case R.id.tv_done:

                saveCroppedImage();


                break;
        }
    }

    private void saveCroppedImage() {
        if (filePath != null) {
            rlProgressBar.setVisibility(View.VISIBLE);
            try {
                String filePathToSaveTo = Methods.createMediaFile(AppConstants.folderPath, ".png").getAbsolutePath();
                Bitmap bmp = ivCrop.getCroppedImage();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp = CameraUtils.correctRotation(filePathToSaveTo, bmp);
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, bos);
                byte[] bitmapData = bos.toByteArray();

                try {
                    FileOutputStream fos = new FileOutputStream(filePathToSaveTo);
                    fos.write(bitmapData);
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (isWithCaption) {
                    Intent intent = new Intent(this, PreviewImageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(BUNDLE_IMAGE_FILE_PATH, filePathToSaveTo);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, REQ_CODE_SEND_IMAGE);
                } else {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString(BUNDLE_IMAGE_FILE_PATH, filePathToSaveTo);
                    intent.putExtras(bundle);
                    rlProgressBar.setVisibility(View.GONE);
                    setResult(RESULT_OK, intent);
                    finish();
                }

            } catch (Exception Ex) {
                Ex.printStackTrace();
            }


            rlProgressBar.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_CODE_SEND_IMAGE) {
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    private void initCrop(String filePath) {
        ivCrop.setImageBitmap(BitmapFactory.decodeFile(filePath));
    }

    private void setupCropImageView() {

        ivCrop.setAspectRatio(getIntent().getIntExtra(ARG_RATIO_X, -1), getIntent().getIntExtra(ARG_RATIO_Y, -1));
        ivCrop.setFixedAspectRatio(getIntent().getIntExtra(ARG_RATIO_X, -1) == -1 ? false : true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
