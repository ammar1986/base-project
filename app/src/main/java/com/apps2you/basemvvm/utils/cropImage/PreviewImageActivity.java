package com.apps2you.basemvvm.utils.cropImage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.apps2you.basemvvm.R;
import com.facebook.drawee.view.SimpleDraweeView;


/**
 * Created by AppsBassel on 10/20/2017.
 */

public class PreviewImageActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = PreviewImageActivity.class.getSimpleName();

    public static final String BUNDLE_IMAGE_FILE_PATH = "image_file_path";
    public static final String BUNDLE_CAPTION = "image_caption";

    private SimpleDraweeView iv;
    private ImageButton btnSend;
    private EditText etCaption;
    private String filePath;

    private RelativeLayout rlProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_imagee);
        iv = (SimpleDraweeView) findViewById(R.id.iv);
        etCaption = (EditText) findViewById(R.id.et_caption);
        btnSend = (ImageButton) findViewById(R.id.btn_send);
        rlProgressBar = (RelativeLayout) findViewById(R.id.rl_progress);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                filePath = bundle.getString(BUNDLE_IMAGE_FILE_PATH);
                iv.setImageURI(Uri.parse(filePath));
            }
        }

        btnSend.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                sendImage();
                break;
        }
    }

    private void sendImage() {
        if (filePath != null) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString(BUNDLE_IMAGE_FILE_PATH, filePath);
            bundle.putString(BUNDLE_CAPTION, etCaption.getText().toString().trim());
            intent.putExtras(bundle);

            rlProgressBar.setVisibility(View.GONE);

            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
