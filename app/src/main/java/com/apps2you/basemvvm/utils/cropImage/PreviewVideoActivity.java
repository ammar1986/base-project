package com.apps2you.basemvvm.utils.cropImage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.apps2you.basemvvm.R;

import fm.jiecao.jcvideoplayer_lib.JCUserAction;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;


/**
 * Created by AppsBassel on 10/19/2017.
 */

public class PreviewVideoActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = PreviewVideoActivity.class.getSimpleName();

    public static final String BUNDLE_VIDEO_FILE_PATH = "image_file_path";
    public static final String BUNDLE_CAPTION = "image_caption";

    private JCVideoPlayerStandard mVideoPlayer;
    private ImageButton btnSend;
    private EditText etCaption;
    private String filePath;

    private RelativeLayout rlProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_video_preview);

        mVideoPlayer = (JCVideoPlayerStandard) findViewById(R.id.video_player);
        mVideoPlayer.battery_level.setVisibility(View.GONE);
        btnSend = (ImageButton) findViewById(R.id.btn_send);
        etCaption = (EditText) findViewById(R.id.et_caption);
        rlProgressBar = (RelativeLayout) findViewById(R.id.rl_progress);

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                filePath = bundle.getString(BUNDLE_VIDEO_FILE_PATH);

                initVideoPlayer(filePath);
            }
        }

        btnSend.setOnClickListener(this);
    }



    private void initVideoPlayer(String filePath) {
        if (filePath != null) {
            mVideoPlayer.setUp(filePath, JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, "");
            mVideoPlayer.thumbImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            mVideoPlayer.setJcUserAction(new JCUserAction() {
                @Override
                public void onEvent(int type, String url, int screen, Object... objects) {
                    switch (type) {
                        case JCUserAction.ON_CLICK_START_ICON:

                            break;
                    }

                }
            });


            MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();
            metadataRetriever.setDataSource(filePath);
            Bitmap bitmap = metadataRetriever.getFrameAtTime(0, MediaMetadataRetriever.OPTION_NEXT_SYNC);
            mVideoPlayer.thumbImageView.setImageBitmap(bitmap);


        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        JCVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_send:
                uploadVideo();
                break;
        }
    }

    private void uploadVideo() {
        rlProgressBar.setVisibility(View.VISIBLE);
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_VIDEO_FILE_PATH, filePath);
        bundle.putString(BUNDLE_CAPTION, etCaption.getText().toString().trim());
        intent.putExtras(bundle);

        rlProgressBar.setVisibility(View.GONE);

        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



}